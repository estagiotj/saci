
<body>
@if(($info['type'] === 'great'))
	<img src="https://media.giphy.com/media/t2eBr71ACeDC0/giphy.gif" />
@else
	<img src="https://thumbs.gfycat.com/PortlyVengefulAxolotl-size_restricted.gif" />
@endif
<h2>{{ $info['message'] }}</h2>
<h4>Você será redirecionado em: <bdi id="time"></bdi></h4>

</body>
<script type="text/javascript">
	function startTimer(duration, display) {
	    var timer = duration, minutes, seconds;
	    setInterval(function () {
	        minutes = parseInt(timer / 60, 10);
	        seconds = parseInt(timer % 60, 10);

	        minutes = minutes < 10 ? "0" + minutes : minutes;
	        seconds = seconds < 10 ? "0" + seconds : seconds;

	        display.textContent = minutes + ":" + seconds;

	        if (--timer < 0) {
	            window.location.pathname="apontamento";
	        }
	    }, 1000);
	}

window.onload = function () {
    display = document.querySelector('#time');
    startTimer(5, display);
};

</script>