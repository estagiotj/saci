@section('header_name')
	PONTO
@stop

@section('action')
	{{ action('ApontamentoController@confirm') }}
@stop

@section('action_button')
	{{trans('adminlte::adminlte.confirm')}}
@stop

@extends('adminlte::login')