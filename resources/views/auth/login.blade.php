@section('header_name')
	LOGIN
@stop

@section('action')
	{{ url(config('adminlte.login_url', 'login')) }}
@stop

@section('action_button')
	{{trans('adminlte::adminlte.sign_in')}}
@stop

@extends('adminlte::login')