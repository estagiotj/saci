<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email ou senha incorretos',
    'throttle' => 'Muitas tentativas de login. Por favor, tente novamente em :seconds segundos',
    'inative'=> 'Usuário inativo. Favor entrar em contato com os desenvolvedores',
    'bloqued'=> 'Usuário bloqueado. Favor entrar em contato com os recursos humanos',
    'cad_pendent'=> 'Usuário possui pendências no seu cadastro. Favor entrar em contato com os recursos humanos',
    'hom_pendent'=> 'Usuário com cadastro não homologado. Favor entrar em contato com os recursos humanos',
    'not_intern'=> 'Somente estagiários podem realizar apontamentos. Você está cadastrado como :role',

    // PONTO
    'great' =>'Seu apontamento foi realizado com sucesso!',
    'not_great' =>'Seu apontamento foi realizado, mas você chegou atrasado! Terá de justificar isso. :/',

];
