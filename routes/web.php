<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/* ---------------------------------------------------------------------------------------- */
/****************** Admin *******************/
Route::get('/admin', 'AdminController@index');

/****************** Estag *******************/
Route::get('/estagiario', 'EstagiarioController@index');

/****************** Diret *******************/
Route::get('/diretor', 'DiretorController@index');

/****************** RH:'( *******************/
Route::get('/rh', 'RHController@index');

/****************** Super *******************/
Route::get('/supervisor', 'SupervisorController@index');
/* ---------------------------------------------------------------------------------------- */


Route::get('/apontamento', 'ApontamentoController@index')->name('ponto');
Route::post('/apontamento/confirmar', 'ApontamentoController@confirm');