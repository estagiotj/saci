<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RHController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role:rh');
	}
    //Index method for Admin Controller
    public function index()
    {
        return view('rh.index');
    }
}
