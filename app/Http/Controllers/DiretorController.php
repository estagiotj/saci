<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiretorController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role:diretor');
	}
    //Index method for Admin Controller
    public function index()
    {
        return view('diretor.index');
    }
}
