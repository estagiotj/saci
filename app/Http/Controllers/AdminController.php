<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role:admin');
	}
    //Index method for Admin Controller
    public function index()
    {
        return view('admin.index',['user'=>Auth::user()]);
    }

    
}
