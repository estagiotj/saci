<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class SupervisorController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role:supervisor');
	}
    //Index method for Admin Controller
    public function index()
    {
        return view('supervisor.index',['user'=>Auth::user()]);
    }

    public function sub(){
        return view('supervisor.index',['user'=>Auth::user()]);   
    }
}
