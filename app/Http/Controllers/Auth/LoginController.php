<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Lang;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function redirectTo()
    {
        $role = Auth::user()->roles()->orderBy('level')->get()->first()->name;
        if($role !== null){
            return ($role);
        }
        return abort(401,'No tenemos este tipo de usante');
    }

    public function username()
    {
        return 'main_email';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $error=null;

        if ($this->myVerifications($request,$error) && $this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request,$error);
    }

    private function myVerifications(Request $request, &$error){
        $fields= $request->only($this->username(), 'password');
        
        $user=User::where($this->username(),$fields[$this->username()])->first();
        $status='';
        
        if($user !== null)
            $status=$user->status()->get()->first()->name;
        else return false;
          
        switch ($status) {
            case 'Ativo':
                return true;
            case 'Bloqueado':
                $error='bloqued';
                return false;
            case 'Pendente de edição':
                $error='cad_pendent';
                return false;
            case 'Pendente de homologação':
                $error='hom_pendent';
                return false;
            case 'Inativo':
                $error='inative';
                return false;
            default:
                $error='Status não encontrado';
                return false;
        }
    }

    protected function sendFailedLoginResponse(Request $request, $error)
    {
        return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => (isset($error) ?  Lang::get('auth.' . $error) : Lang::get('auth.failed'))
                ]);
    }

    // public function authenticate(\Illuminate\Http\Request $request)
    // {
    //     $credentials = $request->only('main_email', 'password');
        
    //     if (Auth::attempt($credentials)) {
    //         return redirect('login');
    //     }else{
    //         dd($request);
    //     }
    // }

    // protected function validateLogin(\Illuminate\Http\Request $request)
    // {
    //     dd($request->main_email);
    //     User::where('main_email',$request->main_email)
    //     $this->validate($request, [
    //         $this->username() => 'required', 'password' => 'required',  =>
    //     ]);
        
    // }
    // protected function credentials(\Illuminate\Http\Request $request) {
    //     $fields= $request->only($this->username(), 'password');
    //     User::where($this->username(),$fields[$this->username()])->first()->status()->get()->first()->name;
        
    //     return array_merge(
    //         $fields,[
    //             User::where($this->username,$fields[$this->username()])->status() => 'Ativo'
    //         ]
    //     );
    // }
}
