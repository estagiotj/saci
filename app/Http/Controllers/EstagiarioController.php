<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class EstagiarioController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role:estagiario');
	}
    //Index method for Admin Controller
    public function index()
    {
        return view('estagiario.index',['user'=>Auth::user()]);
    }
}
