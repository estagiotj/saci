<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Lang;
use App\User;
use App\Appointment;

class ApontamentoController extends Controller
{
	use AuthenticatesUsers;

    public function index()
    {
        return view('apontamento.index');
    }

    protected function redirectTo()
    {
        return "/";
    }

     public function username()
    {
        return 'main_email';
    }

    public function confirm(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $error=null;

        if ($this->myVerifications($request,$error) && $this->attemptLogin($request)) {
            $id = Auth::id();
            Auth::logout();

            Appointment::create([
            	'intern_id' => $id,
            	'appointment_status_id' => 1
            ]);

            $type = 'not_great';
            return view('apontamento.confirm')->with('info',[
            	'type'=>$type,'message'=>Lang::get('auth.' . $type)
            ]);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request,$error);
    }

    private function myVerifications(Request $request, &$error){
        $fields= $request->only($this->username(), 'password');
        
        $user=User::where($this->username(),$fields[$this->username()])->first();
        
        if($user === null)
            return false;
        $status=$user->status()->get()->first()->name;
        
          
        switch ($status) {
            case 'Ativo':
                $role = $user->roles()->orderBy('level')->get()->first()->name;
                if($role === "estagiario")
                	return true;
                $error=['role'=>ucfirst($role)];
                return false;
            case 'Bloqueado':
                $error='bloqued';
                return false;
            case 'Pendente de edição':
                $error='cad_pendent';
                return false;
            case 'Pendente de homologação':
                $error='hom_pendent';
                return false;
            case 'Inativo':
                $error='inative';
                return false;
            default:
                $error='Status não encontrado';
                return false;
        }
    }

    protected function sendFailedLoginResponse(Request $request, $error)
    {
        return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => (isset($error)? 
                    						(is_array($error) ? 
                    							Lang::get('auth.not_intern',['role'=>$error['role']]) : 
                    							Lang::get('auth.' . $error)) : 
                    						Lang::get('auth.failed')
                    					)
                ]);
    }

    // protected function sendLoginResponse(Request $request){
    // 	if(Auth::check()){
    // 		Auth::logout();
    // 	} 
    // 	return true;
    // }
}
