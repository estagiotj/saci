<?php

use Illuminate\Database\Seeder;

use App\AppointmentStatus;

class AppointmentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$statuses = [
						'Válido'=>'Just ... Ok', 
						'Válido por justificativa' => 'Validado após aceite de análise',
						'Pendente de justificativa' => 'Justificativa não realizada', 
						'Pendente de análise'=> 'Justificativa foi feita, mas não analisada', 
						'Não válido' => 'Justificativa não foi aceita',
					];

		foreach ($statuses as $name => $description) {
        	$userStatus = new AppointmentStatus();
        	$userStatus->name=$name;
        	$userStatus->description=$description;
        	$userStatus->save();
        }
    }
}
