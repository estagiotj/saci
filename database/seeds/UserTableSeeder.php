<?php

use Illuminate\Database\Seeder;
use App\UserStatus;
use App\Role;
use App\User;
use App\Intern;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$status= UserStatus::where('name','Ativo')->first();
        $role = Role::where('name', 'admin')->first();

        $user = new User();
        $user->name="SUPER USER DO";
        $user->cpf='00300321277';
        $user->main_email='sudo@tjrn.jus.br';
        $user->registration=$user->cpf;
        $user->password=bcrypt('123');
        $user->status()->associate($status);
        $user->save();
        $user->roles()->attach($role);

        $status= UserStatus::where('name','Ativo')->first();
        $role = Role::where('name', 'supervisor')->first();
        
        $user = new User();
        $user->name="Exemplo1 da Silva";
        $user->cpf='00300321217';
        $user->main_email='exemplo1@tjrn.jus.br';
        $user->registration="f003123";
        $user->password=bcrypt('123');
        $status->users()->save($user);
        $user->save();
        $user->roles()->attach($role);


        $status= UserStatus::where('name','Ativo')->first();
        $role = Role::where('name', 'estagiario')->first();
        
        $user = new User();
        $user->name="Sub humano da Silva";
        $user->cpf='00301321227';
        $user->main_email='subhumano@tjrn.jus.br';
        $user->registration="f003223";
        $user->password=bcrypt('123');
        $user->user_status_id=$status->id;
        $user->save();
        $user->roles()->attach($role);

        $intern = new Intern();
        $intern->id = $user->id;
        $intern->save();

        $status= UserStatus::where('name','Inativo')->first();
        $role = Role::where('name', 'estagiario')->first();
        
        $user = new User();
        $user->name="Anônimo da Silva";
        $user->cpf='10301321227';
        $user->main_email='anon@tjrn.jus.br';
        $user->registration="f003213";
        $user->password=bcrypt('123');
        $user->user_status_id=$status->id;
        $user->save();
        $user->roles()->attach($role);

        $status= UserStatus::where('name','Bloqueado')->first();
        $role = Role::where('name', 'estagiario')->first();
        
        $user = new User();
        $user->name="Bad da Silva";
        $user->cpf='10301121227';
        $user->main_email='bad@tjrn.jus.br';
        $user->registration="f053213";
        $user->password=bcrypt('123');
        $user->user_status_id=$status->id;
        $user->save();
        $user->roles()->attach($role);
    }
}
