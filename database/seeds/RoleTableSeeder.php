<?php
    
use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=['estagiario'=>1,'diretor'=>4,'rh'=>3,'supervisor'=>2,'admin'=>0];
    	
        foreach ($roles as $name => $level) {
        	$role = new Role();
    	    $role->name=$name;
            $role->level=$level;
    	    $role->save();
        }
    }
}
