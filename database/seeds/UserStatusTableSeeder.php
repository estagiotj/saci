<?php

use Illuminate\Database\Seeder;

use App\User;
use App\UserStatus;

class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
        				'Ativo' => 'Usuário está ativo na instituição',
        				'Bloqueado'=>'Usuário está bloqueado temporariamente',
        				'Pendente de edição'=> 'Cadastro necessita de alterações para ser homologado',
        				'Pendente de homologação'=> 'Usuário fez seu cadastro, porém ainda não foi confirmado/homologado',
        				'Inativo'=>'Usuário não tem mais vínculo com a instituição'
        			];
        foreach ($statuses as $name => $description) {
        	$userStatus = new UserStatus;
        	$userStatus->name=$name;
        	$userStatus->description=$description;
        	$userStatus->save();
        }
    }
}
